import java.awt.Robot;
import java.awt.event.*;
robot = java.awt.Robot;

%%%%%%%%%%%%USER VARIBALES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output File Names
outputName = 'FieldDeployment.txt';
outputPictureName = 'FieldDeployment';

% Plot Title
pTitle = 'Predicted Currents'

% Start Date Variables
sDay = '29';
sMonth = '06';
sYear = '2020';
sTime = '07:00';

% End Date Variables
eDay = '30';
eMonth = '06';
eYear = '2020';
eTime = '07:00';

% Location Variable
lat = '48.5155';
lon = '-123.15291';
locationName = 'South Beach'

%toggle for U and V
vFlag = 0;
uFlag = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

startTime = strcat(sDay, '/', sMonth, '/', sYear, " ", sTime);
endTime = strcat(eDay, '/', eMonth, '/', eYear, " ", eTime);
startDate = datetime(strcat(sMonth, '/', sDay, '/', sYear, " ", sTime), 'TimeZone', 'UTC');
startDate.TimeZone = 'America/Los_Angeles';

% Throw Error if wrong Screen Size
requiredSize = [1,1,1920,1080];
size = get(0, 'MonitorPositions');
if ~isequal(size, requiredSize)
    error("Screen Resolution must be 1920x1080")
end

system('start WebTide.lnk');
% Move and Click Mesh
robot.mouseMove(755, 285);
pause(3);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

robot.mouseMove(755, 300);

% Select N Pacific
pause(1);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Select Navigate
robot.mouseMove(700, 285);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Select Coordinates
robot.mouseMove(700, 300);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Select Lat field and Enter Lat
robot.mouseMove(1000, 550);
pause(1);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
pause(1);
keyPress(lat);

%Select Lon Field and Enter Lon
robot.mouseMove(1000, 575);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
keyPress(lon);

% Select Apply
robot.mouseMove(900, 600);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Select Close
robot.mouseMove(1000, 600);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Add Tide Marker
robot.mouseMove(1200, 375);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Edit Parameters
robot.mouseMove(1200, 500);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Start Date
robot.mouseMove(300, 100);
pause(1);

% Triple Click
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Enter Start Time
keyPress(char(startTime));

% End Date
robot.mouseMove(300, 150);
pause(1);

% Triple Click
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Enter End Time
keyPress(char(endTime));

% Select 1 min Interval
% robot.mouseMove(300, 200);
% robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
% robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
% 
% robot.mouseMove(300, 300);
% robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
% robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);


% Press Okay
robot.mouseMove(150, 475);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
pause(1);

% Change to Current
robot.mouseMove(1250, 325);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Run Tide Prediction
robot.mouseMove(1250, 550);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Get Tide Prediction
robot.mouseMove(1250, 600);
pause(15);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
pause(2);

% Save
robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
robot.keyPress(java.awt.event.KeyEvent.VK_S);
robot.keyRelease(java.awt.event.KeyEvent.VK_S);
robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
pause(3);
robot.mouseMove(775, 500);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

% Confirm Save
pause(3);
robot.mouseMove(1000, 530);
robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

system('taskkill /IM WebTide.exe');

pause(10)

filename = "Tidal Current Prediction (Time in GMT).html";
dataLists = py.HTMLManipulation.HTMLedit(filename)

% Create Data Table
UCurrent = zeros(2, 1);
VCurrent = zeros(2, 1);
Longitude = zeros(2, 1);
Latitude = zeros(2, 1);
Year = zeros(2, 1);
JulianDay = zeros(2, 1);
Hour = zeros(2, 1);
Minute = zeros(2, 1);
Second = zeros(2, 1);

index = 1;
listNum = 1;

for i = dataLists
    list = i{1};
    
    UData = nan;
    VData = nan;
    LongData = nan;
    LatData = nan;
    YearData = nan;
    JulianData = nan;
    HourData = nan;
    MinuteData = nan;
    SecondData = nan;
    
    for j = list
        data = j{1};
        
        if index == 1
            UData = data;
        elseif index == 2
            VData = data;
        elseif index == 3
            LongData = data;
        elseif index == 4
            LatData = data;
        elseif index == 5
            YearData = data;
        elseif index == 6
            JulianData = data;
        elseif index == 7
            HourData = data;
        elseif index == 8
            MinuteData = data; 
        elseif index == 9
            SecondData = data;            
        end
        index = index + 1;
    end
    
    UCurrent(listNum) = UData;
    VCurrent(listNum) = VData;
    Longitude(listNum) = LongData;
    Latitude(listNum) = LatData;
    Year(listNum) = YearData;
    JulianDay(listNum) = JulianData;
    Hour(listNum) = HourData;
    Minute(listNum) = MinuteData;
    Second(listNum) = SecondData;
    
    index = 1;
    listNum = listNum + 1;
end
T = table(UCurrent, VCurrent, Longitude, Latitude, Year, JulianDay, Hour, Minute, Second);

% Calc Abs Mag
absMagnitude = zeros(2, 1);
for i = 1:length(T.UCurrent)
    X = T.UCurrent(i)^2;
    Y = T.VCurrent(i)^2;
    absMagnitude(i) = sqrt(X + Y);
end
T = addvars(T, absMagnitude);

% Threshold Calculation
flagged = zeros(2, 1);
for point = 1:length(absMagnitude)
    if absMagnitude(point) <= 0.5
        flagged(point) = absMagnitude(point);
    else
        flagged(point) = 0;
    end
end
flagged(flagged == 0) = NaN;

% Date Time add to Table
dateTimes = startDate;
for i = 1:length(T.absMagnitude)
    dateTimes(i) = (startDate + hours(i - 1));
end
dateTimes2 = dateTimes'
T.dateTimes = dateTimes2



% Abs Mag Plot
x = [0:1:length(T.absMagnitude) - 1];
x1 = T.dateTimes;
y = T.absMagnitude
plot(x1, y);
%xticks(0:12:length(T.absMagnitude));
set(gca,'XGrid','on','YGrid','off')
title(pTitle)
xlabel('Time (Pacific)')
ylabel('Absolute Magnitude (m/s)')
ylim([0 1.5])
%datetick('x', 'HHPM', 'keepticks')
annotation(gcf,'textbox',...
    [0.137458333333333 0.949117341640706 0.1161875 0.0456905503634474],...
    'String',[strcat('Location:', " ", locationName),strcat('Lat:', " ", lat),strcat('Lon:', " ", lon)],...
    'FitBoxToText','off', "EdgeColor",'none');
set(gcf, 'Position', get(0, 'Screensize'));
saveas(gcf, char(strcat(outputPictureName, 'AbsMag', eMonth, eDay, eYear, '.png')))
saveas(gcf, char(strcat(outputPictureName, 'AbsMag', eMonth, eDay, eYear, 'mfig')))

% U Current Plot
if uFlag == 1
    x = [0:1:length(T.UCurrent) - 1];
    x1 = T.dateTimes;
    y = T.UCurrent
    plot(x1, y);
    %xticks(0:12:length(T.absMagnitude));
    set(gca,'XGrid','on','YGrid','off')
    title('Ideal Times for Field Work')
    xlabel('Time (Pacific)')
    ylabel('U Current (m/s)')
    ylim([-1 1])
    yline(0,'k--')
    annotation(gcf,'textbox',...
    [0.137458333333333 0.949117341640706 0.1161875 0.0456905503634474],...
    'String',[strcat('Location:', " ", locationName),strcat('Lat:', " ", lat),strcat('Lon:', " ", lon)],...
    'FitBoxToText','off', "EdgeColor",'none');
    saveas(gcf, char(strcat(outputPictureName, 'UCurrent', eMonth, eDay, eYear, '.png')))
    saveas(gcf, char(strcat(outputPictureName, 'UCurrent', eMonth, eDay, eYear, 'mfig')))
end

% V Current Plot
if vFlag == 1
    x = [0:1:length(T.VCurrent) - 1];
    x1 = T.dateTimes;
    y = T.VCurrent
    plot(x1, y);
    %xticks(0:12:length(T.absMagnitude));
    set(gca,'XGrid','on','YGrid','off')
    title('Ideal Times for Field Work')
    xlabel('Time (Pacific)')
    ylabel('V Current (m/s)')
    ylim([-1 1])
    yline(0,'k--')
    annotation(gcf,'textbox',...
    [0.137458333333333 0.949117341640706 0.1161875 0.0456905503634474],...
    'String',[strcat('Location:', " ", locationName),strcat('Lat:', " ", lat),strcat('Lon:', " ", lon)],...
    'FitBoxToText','off', "EdgeColor",'none');
    saveas(gcf, char(strcat(outputPictureName, 'VCurrent', eMonth, eDay, eYear, '.png')))
    saveas(gcf, char(strcat(outputPictureName, 'VCurrent', eMonth, eDay, eYear, 'mfig')))
end

% Write Text File
inputName = 'CalculatedPrediction.txt';
writetable(T, inputName);
infid = fopen(inputName, 'rt');
outfid = fopen(outputName, 'wt');
fprintf( outfid, 'Calculated using WebTide Version 0.7.1\n' );  %the text you are adding at the beginning
fprintf( outfid, 'Units for currents are meters per second, the time is in UTC\n');
fprintf( outfid, 'The absMagnitude column is the calculated absolute magnitud eof the U and V currents\n');
while true
    thisline = fgetl(infid);    %read line from input file
    if ~ischar(thisline); break; end    %reached end of file
        fprintf( outfid, '%s\n', thisline );   %write the line to the output file
end
fclose(infid);
fclose(outfid);

% Simulates Key Presses

function keyPress(string)
    robot = java.awt.Robot;
    for char = 1:strlength(string)
        if string(char) == '0'
            robot.keyPress(java.awt.event.KeyEvent.VK_0);
            robot.keyRelease(java.awt.event.KeyEvent.VK_0);
        elseif string(char) == '1'
            robot.keyPress(java.awt.event.KeyEvent.VK_1);
            robot.keyRelease(java.awt.event.KeyEvent.VK_1);
        elseif string(char) == '2'
            robot.keyPress(java.awt.event.KeyEvent.VK_2);
            robot.keyRelease(java.awt.event.KeyEvent.VK_2);
        elseif string(char) == '3'
            robot.keyPress(java.awt.event.KeyEvent.VK_3);
            robot.keyRelease(java.awt.event.KeyEvent.VK_3);
        elseif string(char) == '4'
            robot.keyPress(java.awt.event.KeyEvent.VK_4);
            robot.keyRelease(java.awt.event.KeyEvent.VK_4);
        elseif string(char) == '5'
            robot.keyPress(java.awt.event.KeyEvent.VK_5);
            robot.keyRelease(java.awt.event.KeyEvent.VK_5);
        elseif string(char) == '6'
            robot.keyPress(java.awt.event.KeyEvent.VK_6);
            robot.keyRelease(java.awt.event.KeyEvent.VK_6);
        elseif string(char) == '7'
            robot.keyPress(java.awt.event.KeyEvent.VK_7);
            robot.keyRelease(java.awt.event.KeyEvent.VK_7);
        elseif string(char) == '8'
            robot.keyPress(java.awt.event.KeyEvent.VK_8);
            robot.keyRelease(java.awt.event.KeyEvent.VK_8);
        elseif string(char) == '9'
            robot.keyPress(java.awt.event.KeyEvent.VK_9);
            robot.keyRelease(java.awt.event.KeyEvent.VK_9);  
        elseif string(char) == '-'
            robot.keyPress(java.awt.event.KeyEvent.VK_MINUS);
            robot.keyRelease(java.awt.event.KeyEvent.VK_MINUS);
        elseif string(char) == '.'
            robot.keyPress(java.awt.event.KeyEvent.VK_PERIOD);
            robot.keyRelease(java.awt.event.KeyEvent.VK_PERIOD); 
        elseif string(char) == '/'
            robot.keyPress(java.awt.event.KeyEvent.VK_SLASH);
            robot.keyRelease(java.awt.event.KeyEvent.VK_SLASH);
        elseif string(char) == ':'
            robot.keyPress(java.awt.event.KeyEvent.VK_SHIFT);
            robot.keyPress(java.awt.event.KeyEvent.VK_SEMICOLON);
            robot.keyRelease(java.awt.event.KeyEvent.VK_SEMICOLON);
            robot.keyRelease(java.awt.event.KeyEvent.VK_SHIFT);
        elseif string(char) == ' '
            robot.keyPress(java.awt.event.KeyEvent.VK_SPACE);
            robot.keyRelease(java.awt.event.KeyEvent.VK_SPACE);
        end
    end
end