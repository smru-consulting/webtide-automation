#!/usr/bin/env python
# coding: utf-8

import codecs

import codecs

def HTMLedit(filename):
    file = codecs.open(filename, 'r')
    lines = file.readlines()
    lines1 = []
    
    # Delete all /n
    for line in lines:
        line = line.replace("\n", "")
        line = line.replace("</p><pre>", "")
        lines1.append(line)
    
    # Delete Header
    del lines1[0:10]
    del lines1[-1]
    del lines1[-1]
    del lines1[-1]
    
    index = 0
    for line in lines1:
        lines1[index] = line.split()
        index = index + 1
    
    newLists = []
    
    for numList in lines1:
        newList = []
        for data in numList:
            newList.append(float(data))
        newLists.append(newList)
        
    return newLists




