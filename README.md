## Required Files and Programs

**Files:**

- Python Script
- Matlab Script

**Programs:**

- Webtide
- Python 3.7
- Matlab

**How to Download WebTide**

1. Open this link in a web browser: https://www.bio.gc.ca/science/research-recherche/ocean/webtide/download-telecharger-en.php#ver
2. Press the download with java button for the OS you are on.
3. Now press the NorthEast Pacific button under download data sets on the left hand side of the screen.
4. Press the download button corresponding to your OS.
5. Now run both exectuable setup programs for WebTide and the dataset you have just downloaded.

---

## Setup

Display Settings:
- Make sure you're display is set to 1920 x 1080 resolution and 100 percent zoom level. If they are not these settings, the automation will misclick on the program.

Setting up the folder:

1. In desired folder, copy and paste the .py file and matlab script into the folder.

*You can do this by downloading the repository, then extracting all to the desired folder.*

2. Create a shortcut in this folder to the webtide program and make sure the shortcut is named "WebTide"
3. At this point, the webtide shortcut and both scripts should all be in the same folder.

Connecting Python to Matlab:

1. In Matlab, under the home tab at the top of the screen, press the "Set Path" button,
2. Now press the "Add Folder" button.
3. Navigate to where your Python 3 basepath file is and press the "Select Folder" button with the Python 3 folder highlighted.

*The python3 basepath file is usually under the following path: C:\Users\nameofuser\AppData\Local\Programs\Python*

4. Press save to save the basepath to matlab.
5. Exit out of matlab and restart.

## Running the Script

The WebTide Automation script makes WebTide save an HTML output of WebTide's data. Make sure that the HTML is saving to the working/current director that the script is in, otherwise the script will not be able to plot the correct data.

1. Open up script in Matlab.
2. The editable fields are startTime, lat, lon, endTime.
3. When ready, press the run button at the top under the live script tab.

If there is an error with HTMLManipulation.py, the cpython package probably needs to be updated. To do this open up the command line and type "pip install cpython".